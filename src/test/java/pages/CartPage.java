package pages;

import core.TestBase;
import org.apache.log4j.Logger;
import org.testng.Assert;

public class CartPage {

    private static final Logger LOGGER = Logger.getLogger(HomePage.class);

    public void validateCartPageLoaded () {
        String currentUrl = TestBase.driver.getCurrentUrl();
        Assert.assertEquals(currentUrl, "https://www.amazon.com/gp/cart/view.html?ref_=nav_cart");
        LOGGER.info("Cart page has been loaded");
    }
}
