package pages;

import core.TestBase;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    private static final Logger LOGGER = Logger.getLogger(HomePage.class);

    @FindBy(xpath = "//span[@class='nav-cart-icon nav-sprite']")
    private WebElement cartButton;

    public void validateHomePageLoaded() {
        String currentUrl = TestBase.driver.getCurrentUrl();
        Assert.assertEquals(currentUrl, "https://www.amazon.com/");
        LOGGER.info("Amazon homepage has been loaded");
    }

    public void validateUserCanClickOnCartButton() {
        cartButton.click();
        LOGGER.info("Cart button has been clicked");
    }
}
