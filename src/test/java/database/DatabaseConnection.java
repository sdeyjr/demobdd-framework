package database;

import core.TestBase;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection extends TestBase {

    public static Connection connection;

    public static String getPropertyFile(String filepath, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filepath));
        return properties.getProperty(key);
    }

    public static void connectionToSQL() throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/worldOfAutomation";
        String username = getPropertyFile("src/main/resources/databasecredential.properties", "username");
        String password = getPropertyFile("src/main/resources/databasecredential.properties", "password");
        connection = DriverManager.getConnection(url, username, password);
    }

}
