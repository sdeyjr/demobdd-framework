package stepdefinations;

import core.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import pages.CartPage;
import pages.HomePage;

public class StepDef extends TestBase {

    private static final Logger LOGGER = Logger.getLogger(HomePage.class);

    @Given("^the user open the chrome and navigate to https://www\\.amazon\\.com$")
    public void the_user_open_the_chrome_and_navigate_to_https_www_amazon_com() {
        initializeDriver();
        driver.get("https://www.amazon.com");
        LOGGER.info("Opening Amazon homepage");
    }

    @When("^the user can see the amazon homepage$")
    public void the_user_can_see_the_amazon_homepage() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.validateHomePageLoaded();
    }

    @Then("^the user clicks on the cart button$")
    public void the_user_clicks_on_the_cart_button() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.validateUserCanClickOnCartButton();
    }

    @Then("^the user validates cart page is displayed$")
    public void the_user_validates_cart_page_is_displayed() {
        CartPage cartPage = PageFactory.initElements(driver, CartPage.class);
        cartPage.validateCartPageLoaded();
    }

    @Then("^the user closes the browser$")
    public void the_user_closes_the_browser() {
        driver.quit();
        LOGGER.info("Browser has been closed");
    }
}

